import dearpygui.dearpygui as dpg
from model import db

# Функция для получения данных из базы данных
def get_data():
    schs = db(db.sch).select().as_list()
    clss = db(db.cls).select().as_list()
    stds = db(db.std).select().as_list()
    return schs, clss, stds

# schs, clss, stds = get_data()
# print(schs, clss, stds)

# Функция для обновления базы данных
def update_database(data):
    schs, clss, stds = data

    for sch in schs:
        db(db.sch.id == sch['id']).update(name=sch['name'], location=sch['location'])

    for cls in clss:
        db(db.cls.id == cls['id']).update(name=cls['name'])

    for std in stds:
        db(db.std.id == std['id']).update(name=std['name'])

    db.commit()

# Функция для создания интерфейса
def render_interface(schs, clss, stds):
    with dpg.window(tag="Main Window"):
        dpg.add_text("schs:")
        dpg.add_listbox(tag="schs Listbox", items=[sch['name'] for sch in schs], callback=show_sch_details)

        dpg.add_text("clss:")
        dpg.add_listbox(tag="clss Listbox", items=[cls['name'] for cls in clss], callback=show_cls_details)

        dpg.add_text("stds:")
        dpg.add_listbox(tag="stds Listbox", items=[std['name'] for std in stds], callback=show_std_details)

    with dpg.window(tag="Details Window"):
        dpg.add_text("Details:")
        dpg.add_text("Details Text", default_value="Select an item from the listboxes to view details.")

def show_sch_details(sender, data):
    selected_sch_index = dpg.get_value("schs Listbox")
    if selected_sch_index is not None:
        selected_sch = schs[selected_sch_index]
        dpg.set_value("Details Text", f"sch Name: {selected_sch['name']}\nLocation: {selected_sch['location']}")

def show_cls_details(sender, data):
    selected_cls_index = dpg.get_value("clss Listbox")
    if selected_cls_index is not None:
        selected_cls = clss[selected_cls_index]
        dpg.set_value("Details Text", f"cls Name: {selected_cls['name']}")

def show_std_details(sender, data):
    selected_std_index = dpg.get_value("stds Listbox")
    if selected_std_index is not None:
        selected_std = stds[selected_std_index]
        dpg.set_value("Details Text", f"std Name: {selected_std['name']}")

# Получение начальных данных
schs, clss, stds = get_data()

dpg.create_context()

# Отображение интерфейса
with dpg.window(tag="Main Window"):
    render_interface(schs, clss, stds)

# Запуск главного цикла DearPyGui
# dpg.start_dearpygui()


dpg.create_viewport(title='Custom Title', width=600, height=200)
dpg.setup_dearpygui()
dpg.show_viewport()
dpg.set_primary_window("Primary Window", True)
dpg.start_dearpygui()
dpg.destroy_context()

# После завершения работы с интерфейсом обновляем базу данных
update_database(get_data())

# Закрываем соединение с базой данных
db.close()
